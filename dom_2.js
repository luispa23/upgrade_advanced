//2.1 Inserta dinamicamente en un html un div vacio con javascript.
const div = document.querySelector('div')
const divVacio = document.createElement('div')
const textDiv = '2.1 div creado dinamicamente'
divVacio.textContent=textDiv
div.appendChild(divVacio)

// 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
const divSegundo = document.createElement('div')
const paragraph = document.createElement('p')
paragraph.innerHTML = '2.3 soy un parrafo creado dentro de un div creado dinamicamente con js'
document.body.appendChild(divSegundo)
divSegundo.appendChild(paragraph)

// 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
	const divTercero = document.createElement('div')
	document.body.appendChild(divTercero)
	for(i=0;i<7;i++){
		const paragraphSeg = document.createElement('p')
		paragraphSeg.innerHTML = 'somos parrafos creados dentro de un div con un ciclo for'
		divTercero.appendChild(paragraphSeg)
	}

// 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.
const paragraphTer = document.createElement('p')
paragraphTer.innerHTML = ' 2.4 Soy dinamico'
document.body.appendChild(paragraphTer)

// 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
const h2 = document.querySelector(".fn-insert-here")
h2.innerHTML='Wubba Lubba dub dub'

// 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter']

const ul = document.createElement('ul')

for (let a = 0; a < apps.length; a++) {
	const app = apps[a];
	const li = document.createElement('li')
	li.innerHTML = apps[a]
	ul.appendChild(li)	
}document.body.appendChild(ul)

// 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
const removeClass = document.getElementsByClassName('fn-remove-me')

for (let index = 0; index < removeClass.length; ) {
	const element = removeClass[index];
	element.remove()
}



// 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
// Recuerda que no solo puedes insertar elementos con .appendChild.
const div1 = document.createElement('div')
const div2 = document.createElement('div')
div1.classList.add('div1')

const p = document.createElement('p')
p.innerHTML = 'Voy en medio!'
document.body.appendChild(div1)
document.body.appendChild(p)
document.body.appendChild(div2)
/* p.insertAdjacentElement("beforebegin", div1) */



// 2.9 Inserta p con el texto 'Voy dentro!', 
// dentro de todos los div con la clase .fn-insert-here
const divs = document.querySelectorAll('div.fn-insert-here')
  for (let z = 0; z < divs.length; z++) {
	  const nodo = divs[z];
	  const pe = document.createElement('p')
	  pe.innerHTML = 'Voy dentro!'
	  nodo.appendChild(pe)
  }


