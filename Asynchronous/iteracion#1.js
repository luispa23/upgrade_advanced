//----------------------------------------------1.1-------------------------------------------------------------


const getData = () => {
    fetch('https://api.nationalize.io')
        .then((response) => response.json())
        .then(myJson => {
            const myList = myJson.results;
            // list(myList);
            console.log(myJson.results);
            paintData(myList);
        });
}


//----------------------------------------------2.1-------------------------------------------------------------


const addListeners = () => {

    document.getElementById('btn').addEventListener('click', getData);
}

//----------------------------------------------2.3-------------------------------------------------------------

/* 2.3 En base al ejercicio anterior. Crea dinamicamente un elemento  por cada petición
a la api que diga...'El nombre X tiene un Y porciento de ser de Z' etc etc.
EJ: El nombre Pepe tiene un 22 porciento de ser de ET y un 6 porciento de ser
de MZ. */

