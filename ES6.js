
/* Crea una arrow function que tenga dos parametros a y b y
que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre
por consola la suma de los dos parametros.*/
let suma = (a=10, b=5)=> a+b;

console.log(suma())


//1.1 Ejecuta esta función sin pasar ningún parametro
let sumar = ()=> a+b;

console.log(sumar())

//1.2 Ejecuta esta función pasando un solo parametro

let sumas = (a)=> a+b;

console.log(sumas())
//1.3 Ejecuta esta función pasando dos parametros
let sumam = (a , b)=> a+b;

console.log(sumam())
